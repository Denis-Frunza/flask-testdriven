import pytest
from sqlalchemy.exc import IntegrityError

from project import db
from project.api.models import User


def test_add_user_model(test_client, init_database):
    user = User(
        username='justatest',
        email='test@test.com',
        password='greaterthaneight'
    )
    db.session.add(user)
    db.session.commit()

    assert user.id
    assert user.username == 'justatest'
    assert user.email == 'test@test.com'
    assert user.active
    assert user.password


def test_add_user_duplicate_username(test_client, init_database):
    user = User(
        username='justatest',
        email='test@test.com',
        password='greaterthaneight'
    )
    db.session.add(user)
    db.session.commit()

    duplicate_user = User(
        username='justatest',
        email='test@test2.com',
        password='greaterthaneight'
    )

    with pytest.raises(IntegrityError):
        db.session.add(duplicate_user)
        db.session.commit()
        assert "sqlalchemy.exc.IntegrityError" in str(excinfo.value)


def test_add_user_duplicate_email(test_client, init_database):
    user = User(
        username='justatest2',
        email='test@test2.com',
        password='greaterthaneight'
    )
    db.session.add(user)
    db.session.commit()

    duplicate_user = User(
        username='justatest2',
        email='test@test2.com',
        password='greaterthaneight'
    )

    with pytest.raises(IntegrityError):
        db.session.add(duplicate_user)
        db.session.commit()
        assert "sqlalchemy.exc.IntegrityError" in str(excinfo.value)

def test_to_json(test_client, init_database):
    user = User(
        username='justatest4',
        email='test@test4.com',
        password='greaterthaneight'
    )

    assert isinstance(user.to_json(), dict)

def test_passwords_are_random():
    user_1 = User('justatest', 'test@test.com', 'greaterthaneight')
    user_2 = User('justatest2', 'test@test2.com', 'greaterthaneight')

    assert user_1.password != user_2.password

def test_auth_encode_token():
    user = User(
        username='justatest4',
        email='test@test4.com',
        password='greaterthaneight'
    )
    auth_token = user.encode_auth_token(user.id)
    assert isinstance(auth_token, bytes)

def test_auth_decode_token():
    user = User('justatest', 'test@test.com', 'test')
    token = user.encode_auth_token(user.id)
    assert User.decode_auth_token(token) == user.id