import json

from flask import current_app

from project import db
from project.api.models import User


def test_user_registration(test_client, init_database):
    response = test_client.post(
        '/auth/register',
        data=json.dumps({
            'username': 'justatest',
            'email': 'test@test.com',
            'password': '123456',
        }),
        content_type='application/json'
        )

    data = json.loads(response.data.decode())
    assert data['status'] == 'success'
    assert data['message'] == 'Successfully registered.'
    assert data['auth_token']
    assert response.status_code == 201


def test_user_registration_duplicate_email(test_client, init_database):
    new_user = User('test', 'test@test1.com', 'test')

    db.session.add(new_user)
    db.session.commit()

    response = test_client.post(
        '/auth/register',
        data=json.dumps({
            'username': 'test1',
            'email': 'test@test1.com',
            'password': 'test',
        }),
        content_type='application/json'
        )
    data = json.loads(response.data.decode())

    assert response.status_code == 400
    assert data['message'] == 'Sorry. That user already exists.'
    assert data['status'] == 'fail'


def test_user_registration_duplicate_username(test_client, init_database):
    new_user = User('test1', 'test@test2.com', 'test')

    db.session.add(new_user)
    db.session.commit()

    response = test_client.post(
        '/auth/register',
        data=json.dumps({
            'username': 'test1',
            'email': 'test@test3.com',
            'password': 'test',
        }),
        content_type='application/json'
        )
    data = json.loads(response.data.decode())

    assert response.status_code == 400
    assert data['message'] == 'Sorry. That user already exists.'
    assert data['status'] == 'fail'


def test_user_registration_invalid_json(test_client, init_database):
    response = test_client.post(
        '/auth/register',
        data=json.dumps({}),
        content_type='application/json'
        )
    data = json.loads(response.data.decode())

    assert response.status_code == 400
    assert data['message'] == 'Invalid payload.'
    assert data['status'] == 'fail'


def test_user_registration_invalid_json_keys_no_username(test_client, init_database):
    response = test_client.post(
        '/auth/register',
        data=json.dumps({
            'email': 'test@test3.com',
            'password': 'test',
        }),
        content_type='application/json'
        )
    data = json.loads(response.data.decode())

    assert response.status_code == 400
    assert data['message'] == 'Invalid payload.'
    assert data['status'] == 'fail'


def test_user_registration_invalid_json_keys_no_email(test_client, init_database):
    response = test_client.post(
        '/auth/register',
        data=json.dumps({
            'username': 'test2',
            'password': 'test',
        }),
        content_type='application/json'
        )
    data = json.loads(response.data.decode())

    assert response.status_code == 400
    assert data['message'] == 'Invalid payload.'
    assert data['status'] == 'fail'


def test_user_registration_invalid_json_keys_no_password(test_client, init_database):
    response = test_client.post(
        '/auth/register',
        data=json.dumps({
            'username': 'test3',
            'email': 'test@test3.com',
        }),
        content_type='application/json'
        )
    data = json.loads(response.data.decode())

    assert response.status_code == 400
    assert data['message'] == 'Invalid payload.'
    assert data['status'] == 'fail'


def test_registered_user_login(test_client, init_database):
    user = User('test6', 'test@testuser.com', 'test')
    db.session.add(user)
    db.session.commit()

    response = test_client.post(
        '/auth/login',
        data=json.dumps({
            'username': 'test6',
            'email': 'test@testuser.com',
            'password': 'test'
        }),
        content_type='application/json'
        )

    data = json.loads(response.data.decode())

    assert data['status'] == 'success'
    assert data['message'] == 'Successfully logged in.'
    assert data['auth_token']


def test_not_registered_user_login(test_client, init_database):
    response = test_client.post(
        '/auth/login',
        data=json.dumps({
            'username': 'test6',
            'email': 'test@testuser1.com',
            'password': 'test'
        }),
        content_type='application/json'
        )

    data = json.loads(response.data.decode())
    assert data['status'] == 'fail'
    assert data['message'] == "User doesn't exist"
    assert response.status_code == 400


def test_invalid_logout_expired_token(test_client, init_database):
    user = User('test8', 'test@testuser1.com', 'test')
    db.session.add(user)
    db.session.commit()

    current_app.config['TOKEN_EXPIRATION_DAYS'] = -1

    resp_login = test_client.post(
        '/auth/login',
        data=json.dumps({
            'email': 'test@testuser1.com',
            'password': 'test'
        }),
        content_type='application/json',
        )

    token = json.loads(resp_login.data.decode())['auth_token']
    response = test_client.get(
        '/auth/logout',
        headers={'Authorization': f'Bearer {token}'}
        )
    data = json.loads(response.data.decode())

    assert data['status'] == 'fail'
    assert data['message'] == 'Signature has expired'
    assert response.status_code == 401


def test_user_status(test_client, init_database):
    user = User('test9', 'test@testuser9.com', 'test')
    db.session.add(user)
    db.session.commit()

    current_app.config['TOKEN_EXPIRATION_DAYS'] = 30

    response = test_client.post(
        '/auth/login',
        data=json.dumps({
            'name': 'test9',
            'email': 'test@testuser9.com',
            'password': 'test'
        }),
        content_type='application/json'
    )
    token = json.loads(response.data.decode())['auth_token']

    response = test_client.get(
        '/auth/status',
        headers={'Authorization': f'Bearer {token}'}
    )
    data = json.loads(response.data.decode())
    assert data['status'] == 'success'
    assert data['data'] is not None
    assert data['data']['username'] == 'test9'
    assert data['data']['email'] == 'test@testuser9.com'
    assert data['data']['active'] is True
    assert response.status_code == 200


def test_invalid_status(test_client, init_database):
    response = test_client.get(
        '/auth/status',
        headers={'Authorization': 'Bearer invalid'}
    )
    data = json.loads(response.data.decode())
    assert data['status'] == 'fail'
    assert data['message'] == 'Invalid token'
    assert response.status_code == 401
