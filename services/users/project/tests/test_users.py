import json
import time
import pytest
from project import db
from project.api.models import User

def test_add_user(test_client, init_database, token):
    response = test_client.post(
        '/users',
        data=json.dumps({
            'username': 'denis',
            'email': 'test2@gmail.ru',
            'password': 'greaterthaneight'
        }),
        content_type='application/json',
        headers={'Authorization': f'Bearer {token}'}
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 201
    assert 'test2@gmail.ru was added!' in data['message']
    assert 'success' in data['status']


@pytest.mark.parametrize(
    "input_data", [
        {},
        {'email': 'frunzadenis.93@gmail.com'},
    ]
)
def test_add_user_invalid_json(input_data, test_client, init_database, token):
    response = test_client.post(
        '/users',
        data=json.dumps(input_data),
        content_type='application/json',
        headers={'Authorization': f'Bearer {token}'}
    )
    assert response.status_code == 400


def test_add_user_duplicate_email(test_client, init_database, token):
    response = test_client.post(
        '/users',
        data=json.dumps({
            'username': 'denis',
            'email': 'test2@gmail.ru',
            'password': 'greaterthaneight'
        }),
        content_type='application/json',
        headers={'Authorization': f'Bearer {token}'}
    )
    assert response.status_code == 400


def test_single_user(test_client, init_database):
    response = test_client.get(
        '/users/2',
        content_type='application/json'
        )
    data = json.loads(response.data.decode())
    assert response.status_code == 200
    assert 'denis' in data['data'][0]['username']
    assert 'test2@gmail.ru' in data['data'][0]['email']
