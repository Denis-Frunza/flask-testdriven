import json

from flask import current_app

from project import db
from project.api.models import Exercise


def test_add_exercise(test_client, init_database):
    response = test_client.post(
        '/exercises',
        data=json.dumps({
            'body':'Just a sample',
            'test_code':'print("Hello, World!")',
            'test_code_solution':'Hello, World!'
        }),
        content_type='application/json',
        headers=({'Authorization': f'Bearer {token}'})
    )
    data = json.loads(response.data.decode())

    assert response.status_code == 201
    assert data['message'] == 'New exercise was added!'
    assert data['status'] == 'success'


def test_add_exercise_no_header(test_client, init_database)):
    """Ensure error is thrown if 'Authorization' header is empty."""
    response = self.client.post(
        '/exercises',
        data=json.dumps({
            'body': 'Sample sample',
            'test_code': 'get_sum(2, 2)',
            'test_code_solution': '4',
        }),
        content_type='application/json'
    )
    data = json.loads(response.data.decode())

    assert response.status_code == 403
    assert 'Provide a valid auth token.' == data['message']
    assert 'error' == data['status']