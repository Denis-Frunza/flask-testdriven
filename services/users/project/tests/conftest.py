import pytest
import json

from project import create_app, db
from project.api.models import User

@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app()

    flask_app.config.from_object('project.config.TestingConfig')

    with flask_app.test_client() as testing_client:
        with flask_app.app_context():
            yield testing_client


@pytest.fixture(scope='module')
def init_database():
    db.create_all()

    yield db
    db.session.close()
    db.drop_all()


@pytest.fixture(scope='module')
def token():
    user = User('useradmin', 'useradmin@gmail.com', 'test')
    user.admin=True
    db.session.add(user)
    db.session.commit()

    token = user.encode_auth_token(user.id)
    return token.decode()
