from sqlalchemy import exc
from flask import Blueprint, request
from flask_restful import Api, Resource
from project.api.utils import authenticate

from project.api.models import Score
from project import db
scores_blueprint = Blueprint('scores', __name__)
api = Api(scores_blueprint)


class ScoresList(Resource):

    method_decorators = {'post': [authenticate]}

    def post(self, resp):
        """Add score"""
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }
        post_data = request.get_json()
        if not post_data:
            return response_object, 400
        try:
            db.session.add(
                Score(
                    user_id=resp,
                    exercise_id=post_data.get('exercise_id'),
                    correct=post_data.get('correct')
                )
            )
            db.session.commit()
            response_object['status'] = 'success'
            response_object['message'] = 'New score was added!'
            return response_object, 201
        except exc.IntegrityError:
            db.session.rollback()
            return response_object, 400
        except (exc.IntegrityError, ValueError):
            db.session.rollback()
            return response_object, 400

    def get(self):
        """Get all scores"""
        response_object = {
            'status':'success',
            'data': {
                'scores': [
                    score.to_json() for score in Score.query.all()
                ]
            }
        }
        return response_object, 200


class ScoresByUser(Resource):

    method_decorators = {'get': [authenticate]}

    def get(self, resp):
        """Get all scores by user id"""
        scores = Score.query.filter_by(user_id=resp).all()
        response_object = {
            'status':'success',
            'data': {
                'scores': [
                    score.to_json() for score in scores
                ]
            }
        }
        return response_object, 200

class ScoreByUser(Resource):

    method_decorators = {'get': [authenticate]}
    
    def get(self, resp, score_id):
        """Get single score by user id"""

        response_object = {
            'status': 'fail',
            'message': 'Score does not exist'
        }

        try:
            single_score = Score.query.filter_by(
                id=score_id,
                user_id=resp
            ).first()

            if not single_score:
                return response_object, 404
            else:
                response_object = {
                    'status': 'success',
                    'data': single_score.to_json()
                }
                return response_object, 200
        except ValueError:
            return response_object, 404

class ScoreByExercise(Resource):

    method_decorators = {'put':[authenticate]}

    def put(self, user_id, exercise_id):
        """Update score"""
        response_object = {
            'status': 'fail',
            'message': 'invalid payload'
        }

        post_data = request.get_json()
        if not post_data:
            return response_object, 400
        print(exercise_id)
        correct = post_data.get('correct')
        try:
            score = Score.query.filter_by(
                exercise_id=int(exercise_id),
                user_id=user_id
            ).first()
            if score:
                score.correct = correct
                db.session.commit()
                response_object['status'] = 'success'
                response_object['message'] = 'Score was updated!'
                return response_object, 200
            else:
                db.session.add(Score(
                    user_id=user_id,
                    exercise_id= exercise_id,
                    is_correct=correct
                ))
                db.session.commit()
                response_object['status'] = 'success'
                response_object['message'] = 'New score was added!'
                return response_object, 201
        except (exc.IntegrityError, ValueError, TypeError):
            db.session().rollback()
            return response_object, 400


api.add_resource(ScoresList, '/scores')
api.add_resource(ScoresByUser, '/scores/user')
api.add_resource(ScoreByUser, '/scores/user/<score_id>')
api.add_resource(ScoreByExercise, '/scores/<exercise_id>')