from flask import Blueprint, request
from flask_restful import Api, Resource

from project import db
from project.api.utils import authenticate
from project.api.models import Exercise
from sqlalchemy import exc


exercises_blueprint = Blueprint('exercises', __name__)
api = Api(exercises_blueprint)


class AddExercise(Resource):

    method_decorators = {'post': [authenticate]}

    def validate_data(self, post_data):
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }

        if not post_data:
            return response_object, 400
        else:
            response_object, status_code = self.save(post_data)
        return response_object, status_code

    def save(self, post_data):
        body = post_data.get('body')
        test_code = post_data.get('test_code')
        test_code_solution = post_data.get('test_code_solution')

        try:
            exercise = Exercise(
                body=body,
                test_code=test_code,
                test_code_solution=test_code_solution
            )
            db.session.add(exercise)
            db.session.commit()

            response_object = {
                'status': 'success',
                'message': 'New exercise was added!'
            }
            return response_object, 201
        except (exc.IntegrityError, ValueError):
            db.session.rollback()
            return response_object, 400

    def get(self):
        response_object = {
            'status': 'success',
            'data': {
                'exercises': [
                    exercise.to_json() for exercise in Exercise.query.all()
                ]
            }
        }
        return response_object, 200

    def post(self, resp):

        post_data = request.get_json()

        response_object, status_code = self.validate_data(post_data)

        return response_object, status_code



api.add_resource(AddExercise,'/exercises')