from flask import Blueprint, request
from flask_restful import Api, Resource
from sqlalchemy import exc, or_

from project import bcrypt, db
from project.api.models import User
from project.api.utils import authenticate

auth_blueprint = Blueprint('auth', __name__)
api = Api(auth_blueprint)


class RegisterUser(Resource):
    def post(self):
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }
        post_data = request.get_json()
        if not post_data:
            return response_object, 400

        username = post_data.get('username')
        email = post_data.get('email')
        password = post_data.get('password')

        try:
            user = User.query.filter(
                or_(User.username == username, User.email == email)
            ).first()
            if not user:
                new_user = User(
                    username=username,
                    email=email,
                    password=password
                    )
                db.session.add(new_user)
                db.session.commit()

                auth_token = new_user.encode_auth_token(new_user.id)
                response_object['status'] = 'success'
                response_object['message'] = 'Successfully registered.'
                response_object['auth_token'] = auth_token.decode()
                return response_object, 201
            else:
                response_object['message'] = 'Sorry. That user already exists.'
                return response_object, 400
        except (exc.IntegrityError, ValueError):
            db.session.rollback()
            return response_object, 400


class LoginUser(Resource):
    def post(self):
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }

        post_data = request.get_json()
        if not post_data:
            return response_object, 400
        email = post_data.get('email')
        password = post_data.get('password')

        try:
            user = User.query.filter_by(email=email).first()

            if user and bcrypt.check_password_hash(user.password, password):
                auth_token = user.encode_auth_token(user.id)
                if auth_token:
                    response_object['status'] = 'success'
                    response_object['message'] = 'Successfully logged in.'
                    response_object['auth_token'] = auth_token.decode()
                    return response_object, 201
            else:
                response_object['message'] = "User doesn't exist"
                return response_object, 400
        except Exception:
            response_object['message'] = 'Try again.'
            return response_object, 500


class LogoutUser(Resource):
    method_decorators = {'get': [authenticate]}
    
    def get(self):
        response_object = {
            'status': 'success',
            'message': 'Successfully logged out.'
        }
        return response_object, 200


class UserStatus(Resource):
    def get(self):
        auth_header = request.headers.get('Authorization')
        response_object = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }

        if auth_header:
            auth_token = auth_header.split(' ')[1]
            resp = User.decode_auth_token(auth_token)
            if not isinstance(resp, str):
                user = User.query.filter_by(id=resp).first()
                response_object['status'] = 'success'
                response_object['message'] = 'Success.'
                response_object['data'] = user.to_json()
                return response_object, 200
            response_object['message'] = resp
            return response_object, 401
        else:
            return response_object, 501


api.add_resource(RegisterUser, '/auth/register')
api.add_resource(LoginUser, '/auth/login')
api.add_resource(LogoutUser, '/auth/logout')
api.add_resource(UserStatus, '/auth/status')
